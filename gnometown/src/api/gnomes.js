import _ from 'lodash';
import fuseLib from 'fuse.js';

//implemented https://fusejs.io/
export const getGnomes = async () => {
    const result = await fetch('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json')
    const resultsJson = result.json();
    return resultsJson;
};

export const getTownDetails = async () => {
    const gnomes = await getGnomes();
    const friendly = _.filter(gnomes.Brastlewark, function(g) {return g.friends.length > 0;})
    const withoutJob = _.filter(gnomes.Brastlewark, function(g) {return g.professions.length <= 0;})

    const townDetails = {
        gnomes:  gnomes.Brastlewark,
        totalGnomes: gnomes.Brastlewark.length,
        friendlyGnomes: friendly.length,
        withoutJob: withoutJob.length,
     }
    return townDetails;
}; 

export const getGnomesFiltered = async (term) => {
    const gnomes = await getGnomes();
    var options = {
        shouldSort: true,
        threshold: 0.2,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: [
          "name",
          "friends",
          "professions"
        ]
      };
    
    var fuse = new fuseLib(gnomes.Brastlewark, options); 
    return fuse.search(term);
}

export const getGnomeById = async (id) => {
  const gnomes = await getGnomes();
  const gnome = _.find(gnomes.Brastlewark, {'id':id});

  return gnome;
}


