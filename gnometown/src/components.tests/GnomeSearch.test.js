import React from 'react';
import GnomeSearch from '../components/GnomeSearch';
import renderer from 'react-test-renderer';

test('GnomeSearch search gnomes', () => {
    const component = renderer.create(
      <GnomeSearch onFormSubmit={() => {return {}}} gnomesFound={0}/>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });