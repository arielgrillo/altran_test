import React from 'react';
import GnomeDetail from '../components/GnomeDetail';
import renderer from 'react-test-renderer';

test('GnomeDetail render a selected gnome', () => {
    const selectedGnome = {id: 90,
        name: "Kinthony Gyrotink", 
        thumbnail: "http://www.someimages.net/image1.jpg",
        age: 58,
        friends: ["Tinadette Felweaver", "Minabonk Magnapower", "Modwell Gimbaltink"],
        hair_color: "Red",
        height: 117.31269,
        professions: ["Marble Carver", "Brewer", "Butcher"],
        weight: 38.476437
    } 

    const component = renderer.create(
      <GnomeDetail gnome={selectedGnome} />,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });