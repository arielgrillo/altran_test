import React from 'react';
import GnomeTownDetails from '../components/GnomeTownDetails';
import renderer from 'react-test-renderer';

test('GnomeTownDetails search gnomes', () => {
    const component = renderer.create(
      <GnomeTownDetails />,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });