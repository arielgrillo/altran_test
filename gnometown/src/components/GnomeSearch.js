import React from 'react';
import './gnomeSearch.css';


class GnomeSearch extends React.Component {

    state = {term: ''};

    onInputChange = (event) => {
        this.setState({term: event.target.value});
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onFormSubmit(this.state.term);
    }

    renderCount(){        
        if(this.props.gnomesFound > 0){
            return (
                <b>Gnomes found: {this.props.gnomesFound}</b>
            );
        }
        return (
            <b>No gnomes found</b>
        );
    }
    render(){
        return (
            <div className="search-bar ui segment">
            <form onSubmit={this.onFormSubmit} className="ui form">
                <div className="field">
                    <div className="ui grid">
                        <div className="ui row">
                            <div className="eleven wide column">
                                <label>Search (name, friends, profession)</label>
                            </div>
                            <div className ="five wide column gnomes-count">
                                {this.renderCount()}
                            </div>
                        </div>
                    </div>
                    <input 
                        type="text" 
                        value={this.state.term} 
                        onChange = {this.onInputChange}
                    />
                </div>
            </form>
        </div>
        );
    }

}

export default GnomeSearch;