import React from 'react';
import {getTownDetails} from '../api/gnomes';

class GnomeTownDetails extends React.Component {

    state = {townName: 'Brastlewark', townDetails:{}};

    async componentDidMount(){
        const townDetails = await getTownDetails();
        this.setState({townDetails});
    }

    render(){
        return (
            <div className="container ui segment">
                <p><b>Gnomes summary in {this.state.townName}</b></p>
                Total gnomes: <b>{this.state.townDetails.totalGnomes}</b> - 
                Friendly: <b>{this.state.townDetails.friendlyGnomes}</b> - 
                Unemployed: <b>{this.state.townDetails.withoutJob}</b>

            </div>
        );
    }

}

export default GnomeTownDetails;