import React from 'react';
import {isMobile} from 'react-device-detect';

class GnomeDetail extends React.Component{

    setImageWidth(){
        if(isMobile){
            return "gnome-img-mobile";
        }
        return "gnome-img";
    }

    renderGnome(gnome){
        return(
            <div className="ui items">
                <div className={this.setImageWidth()}>
                    <img src={gnome.thumbnail} alt={gnome.name}></img>
                </div>
                <div className="item" key={gnome.id}>
                    <div className="content">
                        <div className="header">{gnome.name}</div>
                        <div className="extra">
                            Age: {gnome.age} - Hair color: {gnome.hair_color} - Height: {gnome.height.toFixed(2)} - Weight: {gnome.weight.toFixed(2)} 
                        </div>
                        <div className="extra">
                        Friends: {gnome.friends.join(', ')}
                        </div>
                        <div className="extra">
                            Professions: {gnome.professions.join(', ')}
                        </div>
                    </div>
                </div>
            </div>
        
        );
    }

    render(){
        if(this.props.gnome){
            return this.renderGnome(this.props.gnome)
        }else{
            return <div></div>
        }
    }
}

export default GnomeDetail;