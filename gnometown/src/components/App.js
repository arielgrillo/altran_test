import React from 'react';
import _ from 'lodash';
import './gnomeItem.css';
import GnomeSearch from './GnomeSearch';
import GnomeTownDetails from './GnomeTownDetails';
import GnomeDetail from './GnomeDetail';
import {getGnomesFiltered, getGnomes, getGnomeById} from '../api/gnomes';

class App extends React.Component {
    
    state = {gnomesFound:[]};

    async componentDidMount() {
        const gnomes = await getGnomes();
        this.setState({gnomesToShow: _.take(gnomes.Brastlewark,5)})
    }

    onTermSubmit = async (term) => {
        const response = await getGnomesFiltered(term);
        this.setState({
            gnomesFound: response,
            selectedGnome: response[0]
        });
    }
    selectGnome = async (id) => {
        const gnome = await getGnomeById(id);
        this.setState({selectedGnome: gnome});
        window.scrollTo(0, 0);
    }
 
    renderList(){
        return this.state.gnomesFound.map((gnome) => {
            return(
                <div className="item gnome-item" key={gnome.id} onClick={() => this.selectGnome(gnome.id)}>
                    <div className="content">
                        <div className="header">{gnome.name}</div>
                        <div className="extra">
                            Friends: {gnome.friends.join(', ')}
                        </div>
                        <div className="extra">
                            Professions: {gnome.professions.join(', ')}
                        </div>
                    </div>
                </div>
            );
        })
    }
    render(){
        return (
            <div className="ui container">
                <GnomeTownDetails />
                <GnomeSearch onFormSubmit={this.onTermSubmit} gnomesFound={this.state.gnomesFound.length}/>
                <div className="ui grid">
                    <div className="ui row">
                        <div className="eight wide column">
                            <div className="ui items">
                                {this.renderList()}
                            </div>
                        </div>
                        <div className="eight wide column">
                            <GnomeDetail gnome={this.state.selectedGnome}/>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default App;